<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Citas */

$this->title = 'Update Citas: ' . $model->IDcit;
$this->params['breadcrumbs'][] = ['label' => 'Citas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IDcit, 'url' => ['view', 'IDcit' => $model->IDcit]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="citas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
