<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Proveedores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IDprove',
            'nombre',
            'localizacion',
            'telefono',
            'categoria',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Proveedores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IDprove' => $model->IDprove]);
                 }
            ],
        ],
    ]); ?>


</div>
