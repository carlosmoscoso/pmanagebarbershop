<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Servicios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Servicios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IDserv',
            'nombre',
            'coste',
            'categoria',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Servicios $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IDserv' => $model->IDserv]);
                 }
            ],
        ],
    ]); ?>


</div>
