<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Piden */

$this->title = 'Create Piden';
$this->params['breadcrumbs'][] = ['label' => 'Pidens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="piden-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
