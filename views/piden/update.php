<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Piden */

$this->title = 'Update Piden: ' . $model->IDpid;
$this->params['breadcrumbs'][] = ['label' => 'Pidens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IDpid, 'url' => ['view', 'IDpid' => $model->IDpid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="piden-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
