<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Piden */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="piden-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'IDclientes')->textInput() ?>

    <?= $form->field($model, 'IDcitas')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
