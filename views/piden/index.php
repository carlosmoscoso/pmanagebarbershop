<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pidens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="piden-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Piden', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IDpid',
            'IDclientes',
            'IDcitas',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Piden $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IDpid' => $model->IDpid]);
                 }
            ],
        ],
    ]); ?>


</div>
