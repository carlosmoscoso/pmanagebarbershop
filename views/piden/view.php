<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Piden */

$this->title = $model->IDpid;
$this->params['breadcrumbs'][] = ['label' => 'Pidens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="piden-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'IDpid' => $model->IDpid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IDpid' => $model->IDpid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IDpid',
            'IDclientes',
            'IDcitas',
        ],
    ]) ?>

</div>
