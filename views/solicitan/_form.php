<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Solicitan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'IDclientes')->textInput() ?>

    <?= $form->field($model, 'IDservicios')->textInput() ?>

    <?= $form->field($model, 'metodoPago')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
