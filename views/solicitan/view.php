<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Solicitan */

$this->title = $model->IDsoli;
$this->params['breadcrumbs'][] = ['label' => 'Solicitans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="solicitan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'IDsoli' => $model->IDsoli], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IDsoli' => $model->IDsoli], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IDsoli',
            'IDclientes',
            'IDservicios',
            'metodoPago',
        ],
    ]) ?>

</div>
