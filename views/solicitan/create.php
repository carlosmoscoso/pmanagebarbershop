<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Solicitan */

$this->title = 'Create Solicitan';
$this->params['breadcrumbs'][] = ['label' => 'Solicitans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
