<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Solicitan */

$this->title = 'Update Solicitan: ' . $model->IDsoli;
$this->params['breadcrumbs'][] = ['label' => 'Solicitans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IDsoli, 'url' => ['view', 'IDsoli' => $model->IDsoli]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="solicitan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
