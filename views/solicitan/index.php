<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Solicitans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Solicitan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IDsoli',
            'IDclientes',
            'IDservicios',
            'metodoPago',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Solicitan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IDsoli' => $model->IDsoli]);
                 }
            ],
        ],
    ]); ?>


</div>
