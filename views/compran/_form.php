<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="compran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'IDproductos')->textInput() ?>

    <?= $form->field($model, 'IDclientes')->textInput() ?>

    <?= $form->field($model, 'metodoPago')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
