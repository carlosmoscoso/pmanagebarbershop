<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Telefonos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telefonos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Telefonos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IDtel',
            'IDclientes',
            'teléfonos',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Telefonos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IDtel' => $model->IDtel]);
                 }
            ],
        ],
    ]); ?>


</div>
