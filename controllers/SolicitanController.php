<?php

namespace app\controllers;

use app\models\Solicitan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SolicitanController implements the CRUD actions for Solicitan model.
 */
class SolicitanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Solicitan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Solicitan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'IDsoli' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Solicitan model.
     * @param int $IDsoli I Dsoli
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IDsoli)
    {
        return $this->render('view', [
            'model' => $this->findModel($IDsoli),
        ]);
    }

    /**
     * Creates a new Solicitan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Solicitan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'IDsoli' => $model->IDsoli]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Solicitan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IDsoli I Dsoli
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IDsoli)
    {
        $model = $this->findModel($IDsoli);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IDsoli' => $model->IDsoli]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Solicitan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IDsoli I Dsoli
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IDsoli)
    {
        $this->findModel($IDsoli)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Solicitan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IDsoli I Dsoli
     * @return Solicitan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IDsoli)
    {
        if (($model = Solicitan::findOne(['IDsoli' => $IDsoli])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
