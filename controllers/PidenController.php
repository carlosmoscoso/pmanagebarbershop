<?php

namespace app\controllers;

use app\models\Piden;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PidenController implements the CRUD actions for Piden model.
 */
class PidenController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Piden models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Piden::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'IDpid' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Piden model.
     * @param int $IDpid I Dpid
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IDpid)
    {
        return $this->render('view', [
            'model' => $this->findModel($IDpid),
        ]);
    }

    /**
     * Creates a new Piden model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Piden();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'IDpid' => $model->IDpid]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Piden model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IDpid I Dpid
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IDpid)
    {
        $model = $this->findModel($IDpid);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IDpid' => $model->IDpid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Piden model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IDpid I Dpid
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IDpid)
    {
        $this->findModel($IDpid)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Piden model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IDpid I Dpid
     * @return Piden the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IDpid)
    {
        if (($model = Piden::findOne(['IDpid' => $IDpid])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
