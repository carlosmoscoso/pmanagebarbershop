<?php

namespace app\controllers;

use app\models\Compran;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompranController implements the CRUD actions for Compran model.
 */
class CompranController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Compran models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Compran::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'IDcomp' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Compran model.
     * @param int $IDcomp I Dcomp
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IDcomp)
    {
        return $this->render('view', [
            'model' => $this->findModel($IDcomp),
        ]);
    }

    /**
     * Creates a new Compran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Compran();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'IDcomp' => $model->IDcomp]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Compran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IDcomp I Dcomp
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IDcomp)
    {
        $model = $this->findModel($IDcomp);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IDcomp' => $model->IDcomp]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Compran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IDcomp I Dcomp
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IDcomp)
    {
        $this->findModel($IDcomp)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Compran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IDcomp I Dcomp
     * @return Compran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IDcomp)
    {
        if (($model = Compran::findOne(['IDcomp' => $IDcomp])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
