<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicios".
 *
 * @property int $IDserv
 * @property string $nombre
 * @property float $coste
 * @property string $categoria
 *
 * @property Solicitan[] $solicitans
 */
class Servicios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'coste', 'categoria'], 'required'],
            [['coste'], 'number'],
            [['nombre', 'categoria'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDserv' => 'I Dserv',
            'nombre' => 'Nombre',
            'coste' => 'Coste',
            'categoria' => 'Categoria',
        ];
    }

    /**
     * Gets query for [[Solicitans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitans()
    {
        return $this->hasMany(Solicitan::className(), ['IDservicios' => 'IDserv']);
    }
}
