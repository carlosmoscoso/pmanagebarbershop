<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $IDclie
 * @property string $nombre
 * @property int|null $citasMensuales
 *
 * @property Compran[] $comprans
 * @property Piden[] $pidens
 * @property Solicitan[] $solicitans
 * @property Telefonos[] $telefonos
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['citasMensuales'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDclie' => 'I Dclie',
            'nombre' => 'Nombre',
            'citasMensuales' => 'Citas Mensuales',
        ];
    }

    /**
     * Gets query for [[Comprans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprans()
    {
        return $this->hasMany(Compran::className(), ['IDclientes' => 'IDclie']);
    }

    /**
     * Gets query for [[Pidens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPidens()
    {
        return $this->hasMany(Piden::className(), ['IDclientes' => 'IDclie']);
    }

    /**
     * Gets query for [[Solicitans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitans()
    {
        return $this->hasMany(Solicitan::className(), ['IDclientes' => 'IDclie']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::className(), ['IDclientes' => 'IDclie']);
    }
}
