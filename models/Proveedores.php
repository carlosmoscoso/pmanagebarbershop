<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property int $IDprove
 * @property string $nombre
 * @property string $localizacion
 * @property string $telefono
 * @property string $categoria
 *
 * @property Productos[] $productos
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'localizacion', 'telefono', 'categoria'], 'required'],
            [['nombre'], 'string', 'max' => 20],
            [['localizacion', 'categoria'], 'string', 'max' => 50],
            [['telefono'], 'string', 'max' => 9],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDprove' => 'I Dprove',
            'nombre' => 'Nombre',
            'localizacion' => 'Localizacion',
            'telefono' => 'Telefono',
            'categoria' => 'Categoria',
        ];
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['IDproveedores' => 'IDprove']);
    }
}
