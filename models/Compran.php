<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compran".
 *
 * @property int $IDcomp
 * @property int|null $IDproductos
 * @property int|null $IDclientes
 * @property string $metodoPago
 *
 * @property Clientes $iDclientes
 * @property Productos $iDproductos
 */
class Compran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IDproductos', 'IDclientes'], 'integer'],
            [['metodoPago'], 'required'],
            [['metodoPago'], 'string', 'max' => 50],
            [['IDclientes'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['IDclientes' => 'IDclie']],
            [['IDproductos'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['IDproductos' => 'IDprod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDcomp' => 'I Dcomp',
            'IDproductos' => 'I Dproductos',
            'IDclientes' => 'I Dclientes',
            'metodoPago' => 'Metodo Pago',
        ];
    }

    /**
     * Gets query for [[IDclientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDclientes()
    {
        return $this->hasOne(Clientes::className(), ['IDclie' => 'IDclientes']);
    }

    /**
     * Gets query for [[IDproductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDproductos()
    {
        return $this->hasOne(Productos::className(), ['IDprod' => 'IDproductos']);
    }
}
