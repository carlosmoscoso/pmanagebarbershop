<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "citas".
 *
 * @property int $IDcit
 * @property string $fecha
 * @property string $hora
 * @property float $coste
 * @property string $categoria
 *
 * @property Piden[] $pidens
 */
class Citas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'citas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora', 'coste', 'categoria'], 'required'],
            [['fecha', 'hora'], 'safe'],
            [['coste'], 'number'],
            [['categoria'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDcit' => 'I Dcit',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'coste' => 'Coste',
            'categoria' => 'Categoria',
        ];
    }

    /**
     * Gets query for [[Pidens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPidens()
    {
        return $this->hasMany(Piden::className(), ['IDcitas' => 'IDcit']);
    }
}
