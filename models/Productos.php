<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $IDprod
 * @property string $nombre
 * @property float $costeCliente
 * @property float $costeProveedores
 * @property string $categoria
 * @property string $metodoPago
 * @property int|null $IDproveedores
 *
 * @property Compran[] $comprans
 * @property Proveedores $iDproveedores
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'costeCliente', 'costeProveedores', 'categoria', 'metodoPago'], 'required'],
            [['costeCliente', 'costeProveedores'], 'number'],
            [['IDproveedores'], 'integer'],
            [['nombre', 'categoria', 'metodoPago'], 'string', 'max' => 50],
            [['IDproveedores'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['IDproveedores' => 'IDprove']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDprod' => 'I Dprod',
            'nombre' => 'Nombre',
            'costeCliente' => 'Coste Cliente',
            'costeProveedores' => 'Coste Proveedores',
            'categoria' => 'Categoria',
            'metodoPago' => 'Metodo Pago',
            'IDproveedores' => 'I Dproveedores',
        ];
    }

    /**
     * Gets query for [[Comprans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprans()
    {
        return $this->hasMany(Compran::className(), ['IDproductos' => 'IDprod']);
    }

    /**
     * Gets query for [[IDproveedores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDproveedores()
    {
        return $this->hasOne(Proveedores::className(), ['IDprove' => 'IDproveedores']);
    }
}
