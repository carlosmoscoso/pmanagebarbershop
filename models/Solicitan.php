<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitan".
 *
 * @property int $IDsoli
 * @property int|null $IDclientes
 * @property int|null $IDservicios
 * @property string $metodoPago
 *
 * @property Clientes $iDclientes
 * @property Servicios $iDservicios
 */
class Solicitan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'solicitan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IDclientes', 'IDservicios'], 'integer'],
            [['metodoPago'], 'required'],
            [['metodoPago'], 'string', 'max' => 50],
            [['IDclientes'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['IDclientes' => 'IDclie']],
            [['IDservicios'], 'exist', 'skipOnError' => true, 'targetClass' => Servicios::className(), 'targetAttribute' => ['IDservicios' => 'IDserv']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDsoli' => 'I Dsoli',
            'IDclientes' => 'I Dclientes',
            'IDservicios' => 'I Dservicios',
            'metodoPago' => 'Metodo Pago',
        ];
    }

    /**
     * Gets query for [[IDclientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDclientes()
    {
        return $this->hasOne(Clientes::className(), ['IDclie' => 'IDclientes']);
    }

    /**
     * Gets query for [[IDservicios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDservicios()
    {
        return $this->hasOne(Servicios::className(), ['IDserv' => 'IDservicios']);
    }
}
