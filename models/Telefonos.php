<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $IDtel
 * @property int|null $IDclientes
 * @property string $teléfonos
 *
 * @property Clientes $iDclientes
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IDclientes'], 'integer'],
            [['teléfonos'], 'required'],
            [['teléfonos'], 'string', 'max' => 9],
            [['IDclientes'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['IDclientes' => 'IDclie']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDtel' => 'I Dtel',
            'IDclientes' => 'I Dclientes',
            'teléfonos' => 'Teléfonos',
        ];
    }

    /**
     * Gets query for [[IDclientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDclientes()
    {
        return $this->hasOne(Clientes::className(), ['IDclie' => 'IDclientes']);
    }
}
