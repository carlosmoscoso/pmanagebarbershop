<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "piden".
 *
 * @property int $IDpid
 * @property int|null $IDclientes
 * @property int|null $IDcitas
 *
 * @property Citas $iDcitas
 * @property Clientes $iDclientes
 */
class Piden extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piden';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IDclientes', 'IDcitas'], 'integer'],
            [['IDcitas'], 'exist', 'skipOnError' => true, 'targetClass' => Citas::className(), 'targetAttribute' => ['IDcitas' => 'IDcit']],
            [['IDclientes'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['IDclientes' => 'IDclie']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDpid' => 'I Dpid',
            'IDclientes' => 'I Dclientes',
            'IDcitas' => 'I Dcitas',
        ];
    }

    /**
     * Gets query for [[IDcitas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDcitas()
    {
        return $this->hasOne(Citas::className(), ['IDcit' => 'IDcitas']);
    }

    /**
     * Gets query for [[IDclientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDclientes()
    {
        return $this->hasOne(Clientes::className(), ['IDclie' => 'IDclientes']);
    }
}
